Rails.application.routes.draw do
  # account management
  post "/register", to: "users#create"
  post "/login", to: "users#login"
  get "/auto_login", to: "users#auto_login"

  # avatar
  post "/add_avatar", to: "users#add_avatar"
  get "/get_avatar_link", to: "users#get_avatar_link"

  # friendship
  get "/friends", to: "friendship#get_friends"
  get "/pending_friends", to: "friendship#get_pending_friends"
  get "/requested_friends", to: "friendship#get_requested_friends"
  post "/send_request", to: "friendship#send_request"
  post "/accept_request", to: "friendship#accept_request"
  post "/decline_request", to: "friendship#decline_request"
  post "/remove_friend", to: "friendship#remove_friend"

  # messages
  get "/messages", to: "users#get_messages"
  post "/message", to: "users#send_message"
  delete "/message", to: "users#remove_message"

  # cable
  mount ActionCable.server => '/cable'
end
