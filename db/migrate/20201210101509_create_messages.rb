class CreateMessages < ActiveRecord::Migration[6.0]
  def change
    create_table :messages do |t|
      t.string :data
      t.string :mime_type
      t.integer :duration

      t.timestamps
    end
  end
end
