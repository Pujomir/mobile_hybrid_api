class ChangeAvatarToBase64Field < ActiveRecord::Migration[6.0]
  def change
    drop_table :active_storage_blobs, force: :cascade
    drop_table :active_storage_attachments, force: :cascade
    add_column :users, :avatar, :string
  end
end
