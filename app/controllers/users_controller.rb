class UsersController < ApplicationController
  before_action :authorized, except: [:create, :login]
  before_action :user_params, only: [:create]

  # REGISTER
  def create
    @user = User.create(@permitted_parmas)
    if @user.valid?
      token = encode_token({user_id: @user.id})
      render json: {user: @user.as_json(except: [:avatar]), token: token}
    else
      render json: {error: "Invalid username or password"}, status: :not_acceptable
    end
  end

  # LOGGING IN
  def login
    @user = User.find_by(username: params[:username]) || User.find_by(email: params[:email])

    if @user && @user.authenticate(params[:password])
      token = encode_token({user_id: @user.id})
      render json: {user: @user.as_json(except: [:avatar]), token: token}
    else
      render json: {error: "Invalid username or password"}, status: :not_acceptable
    end
  end

  # ADD AVATAR
  def add_avatar
    permitted_params = params.permit(:avatar)
    begin
      @user.update(avatar: params[:avatar])
    rescue Exception => error
      render json: {error: error.messge}, status: :not_acceptable
    end
  end

  # GET AVATAR
  def get_avatar_link
    render json: { avatar: @user.avatar }
  end

  # SEND MESSAGE
  def send_message
    begin
      other_user = User.find(params[:id])
      message = Message.create(from_id: @user.id, to_id: params[:id], data: params[:data])
    rescue Exception => error
      render json: {error: error.message}, status: :not_found
    end
  end

  # REMOVE MESSAGE
  def remove_message
    begin
      message = Message.find(params[:id])
      message.destroy
    rescue Exception => error
      render json: {error: error.message}, status: :not_found
    end
  end

  # GET MESSAGES
  def get_messages
    render json: { messages: @user.received_messages }
  end

  def auto_login
    render json: @user
  end

  private

  def user_params
    @permitted_parmas = params.permit(:username, :email, :password)
  end
end
