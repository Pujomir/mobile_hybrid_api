class MessagesChannel < ApplicationCable::Channel
    def subscribed
        @user = User.find_by(username: params[:username])
        stream_for @user
    end
end