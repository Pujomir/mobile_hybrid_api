class User < ApplicationRecord
    has_secure_password
    has_friendship

    has_many :received_messages, foreign_key: "to_id", class_name: "Message"
    has_many :sent_messages, foreign_key: "from_id", class_name: "Message"

    validates :username, :email, presence: true
    validates :username, :email, uniqueness: true

    def as_json(options={})
        exceptions = options[:except] || []
        exceptions.concat([:password_digest])
        options = {:except => exceptions}.merge(options)
        user = super(options)
    end
end
