class NewMessageJob < ApplicationJob
  def perform(message)
    user = User.find(message.to_id)
    MessagesChannel.broadcast_to(user, { message: message.as_json })
  end
end